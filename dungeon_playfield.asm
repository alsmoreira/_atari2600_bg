; Assembler should use basic 6502 instructions
	processor 6502
	
; Include files for Atari 2600 constants and handy macro routines
	include "vcs.h"
	include "macro.h"
	
; Here we're going to introduce the 6502 (the CPU) and
; the TIA (the chip that generates the video signal).
; There's no frame buffer, so you have to program the TIA
; before (or during) each scanline.
; We're just going to initialize the system and put some
; color on the TV.
	seg
; 4K Atari 2600 ROMs usually start at address $F000
	org  $f000

Reset:
	CLEAN_START
        
        ldx #$07	;black background color
        stx COLUBK	
        
        lda #$1C	;gray playfield color
        sta COLUPF
        
;new frame configuring VBLANK e VSYNC
StartFrame:
	lda #$02		;mudar
        sta VBLANK
        sta VSYNC
        
;generate the three lines of VSYNC
	REPEAT 3
        	sta WSYNC ;three  scanlines for VSYNC
        REPEND
        lda #0
        sta VSYNC ;turnoff VSYNC

;let the TIA output the 37 recommended lines of VBLANK
	REPEAT 37
        	sta WSYNC
        REPEND
        lda #0
        sta VBLANK

;set the CTRLPF register to allow playfield reflection
	ldx #$01   ; D0 reflect the PF
        stx CTRLPF

;draw the 192 visible scanlines

	ldx #%11110000
        stx PF0
        ldx #%11111110
        stx PF1
        ldx #%00010101
        stx PF2
        REPEAT 14
        	sta WSYNC
        REPEND

        ldx #%00010000
        stx PF0
        ldx #%00000011
        stx PF1
        ldx #$FF
        stx PF2
        REPEAT 21
        	sta WSYNC
        REPEND
       
;output 30 scanlines more VBLANK to complete frame
	lda #$02
        sta VBLANK
        REPEAT 30
        	sta WSYNC
        REPEND
        lda #$00
        sta VBLANK
        
;loop next frame
	jmp StartFrame

; Here we skip to address $FFFC and define a word with the
; address of where the CPU should start fetching instructions.
; This also fills out the ROM size to $1000 (4k) bytes
	org $fffc
	.word Reset
	.word Reset