; Assembler should use basic 6502 instructions
	processor 6502
	
; Include files for Atari 2600 constants and handy macro routines
	include "vcs.h"
	include "macro.h"
	
; Here we're going to introduce the 6502 (the CPU) and
; the TIA (the chip that generates the video signal).
; There's no frame buffer, so you have to program the TIA
; before (or during) each scanline.
; We're just going to initialize the system and put some
; color on the TV.
	seg
; 4K Atari 2600 ROMs usually start at address $F000
	org  $f000

Reset:
	CLEAN_START
        
        ldx #$81	;blue background color
        stx COLUBK	
        
        lda #$1C	;yellow playfield color
        sta COLUPF
        
;new frame configuring VBLANK e VSYNC
StartFrame:
	lda #$02		;mudar
        sta VBLANK
        sta VSYNC
        
;generate the three lines of VSYNC
	REPEAT 3
        	sta WSYNC ;three  scanlines for VSYNC
        REPEND
        lda #0
        sta VSYNC ;turnoff VSYNC

;let the TIA output the 37 recommended lines of VBLANK
	REPEAT 37
        	sta WSYNC
        REPEND
        lda #0
        sta VBLANK

;set the CTRLPF register to allow playfield reflection
	ldx #$01   ; D0 reflect the PF
        stx CTRLPF

;draw the 192 visible scanlines
;first 7 scanlines skip
	ldx #$00
        stx PF0
        stx PF1
        stx PF2
        REPEAT 7
        	sta WSYNC
        REPEND
;set the PF0 to 1110 and the others PF1 and PF2 to $FF
	ldx #%11100000
        stx PF0
        ldx #$FF
        stx PF1
        stx PF2
        REPEAT 7
        	sta WSYNC
        REPEND
;set the next 164 lines only with PF0 0010
	ldx #%01100000
        stx PF0
        ldx #$00
        stx PF1
        ldx #%10000000
        stx PF2
        REPEAT 164
        	sta WSYNC
        REPEND
;set the PF0 to 1110 and the others PF1 and PF2 to $FF
	ldx #%11100000
        stx PF0
        ldx #$FF
        stx PF1
        stx PF2
        REPEAT 7
        	sta WSYNC
        REPEND
        
;finish the last 7 scanlines same as first 7 scanlines
	ldx #$00
        stx PF0
        stx PF1
        stx PF2
        REPEAT 7
        	sta WSYNC
        REPEND
       
;output 30 scanlines more VBLANK to complete frame
	lda #$02
        sta VBLANK
        REPEAT 30
        	sta WSYNC
        REPEND
        lda #$00
        sta VBLANK
        
;loop next frame
	jmp StartFrame

; Here we skip to address $FFFC and define a word with the
; address of where the CPU should start fetching instructions.
; This also fills out the ROM size to $1000 (4k) bytes
	org $fffc
	.word Reset
	.word Reset